Citation socrate :
Tout ce que je sais  c'est que je ne sais rien, tandis que les autres croient savoir ce qui ne savent pas
Existe-il pour l'homme un bien plus précieux que la santé?
Je suis ni Athénien, ni Grec mais un citoyen du monde
Connais-toi toi-même
L'homme doit s'élever au-dessus de la terre - aux limites de l'atmosphère et au-delà - ainsi seulement pourra-t-il comprendre tout a fait le monde dans lequel il vit
La plus intelligente est celle qui sait qui ne sait pas
Les gens qu'on interroge, pourvu qu'on les interroge bien, trouvent d'eux-mêmes les bonnes réponses.
Une vie sans examen ne vaut pas la peine d'être vécue.
Nul n'est méchant volontairement.
Existe-t-il pour l'homme un bien plus précieux que la Santé?
L'homme est le seul des animaux à croire à des dieux.
